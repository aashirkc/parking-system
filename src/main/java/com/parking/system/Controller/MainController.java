package com.parking.system.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.parking.system.Entities.Details;
import com.parking.system.Services.DetailsService;

@Controller
public class MainController {
	
	@Autowired
	private DetailsService detailsService;

	@RequestMapping(value= "/main")
	public ModelAndView detailss() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("detailss",detailsService.findAll());
		modelAndView.setViewName("details/index");
		return modelAndView;
		
	}
	@GetMapping(value = "/create")
	public ModelAndView createdetails(Details details) {
		
		ModelAndView modelAndView = new ModelAndView();		
		modelAndView.setViewName("details/create");
		return modelAndView;
	}
	
	@PostMapping(value = "/save")
	public String savedetails(@Valid Details details, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "details/create";
		}
		detailsService.save(details);
		return "redirect:/details";
	}
	
	@GetMapping(value = "/update")
	public ModelAndView updatedetails(@RequestParam int id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("details", detailsService.findDetails(id));
		
		modelAndView.setViewName("details/update");
		return modelAndView;
	}
	
	@GetMapping(value = "/view")
	public ModelAndView viewdetails(@RequestParam int id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("details", detailsService.findDetails(id));
		
		modelAndView.setViewName("details/view");
		return modelAndView;
	}
	
	@GetMapping(value = "/delete")
	public ModelAndView deletedetails(@RequestParam int id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/details");
		detailsService.delete(id);
		return modelAndView;
	}
	
}
