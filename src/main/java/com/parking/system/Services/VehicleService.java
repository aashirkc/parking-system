package com.parking.system.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.system.Entities.Vehicle;
import com.parking.system.Repository.VehicleRepository;

@Service
public class VehicleService {
	
	@Autowired
	private VehicleRepository vehicleRepository;
	
	public VehicleService( VehicleRepository vehicleRepository) {
		this.vehicleRepository=vehicleRepository;
	}
	
	public List<Vehicle> findAll(){
		List<Vehicle> vehicles = new ArrayList<>();
		vehicles= vehicleRepository.findAll();
		return vehicles;
	}
	
	public Vehicle findVehicle(int id) {
		return vehicleRepository.findById(id).orElse(null);
	}
	
	public void save(Vehicle vehicle) {
		vehicleRepository.save(vehicle);
	}
	
	public void delete(int id) {
		vehicleRepository.deleteById(id);
	}

}
