package com.parking.system.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.system.Entities.Rate;
import com.parking.system.Repository.RateRepository;

@Service
public class RateService {
	
	@Autowired
	private RateRepository rateRepository;
	
	public RateService( RateRepository rateRepository) {
		this.rateRepository=rateRepository;
	}
	
	public List<Rate> findAll(){
		List<Rate> rateDetails = new ArrayList<>();
		rateDetails= rateRepository.findAll();
		return rateDetails;
	}
	
	public Rate findoutDetail(int id) {
		return rateRepository.findById(id).orElse(null);
	}
	
	public void save(Rate rateDetail) {
		rateRepository.save(rateDetail);
	}
	
	public void delete(int id) {
		rateRepository.deleteById(id);
	}

}
