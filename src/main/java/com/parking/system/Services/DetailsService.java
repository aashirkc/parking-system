package com.parking.system.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.system.Entities.Details;
import com.parking.system.Repository.DetailsRepository;

@Service
public class DetailsService {
	
	@Autowired
	private DetailsRepository detailsRepository;
	
	public DetailsService( DetailsRepository detailsRepository) {
		this.detailsRepository=detailsRepository;
	}
	
	public List<Details> findAll(){
		List<Details> details = new ArrayList<>();
		details= detailsRepository.findAll();
		return details;
	}
	
	public Details findDetails(int id) {
		return detailsRepository.findById(id).orElse(null);
	}
	
	public void save(Details timeDetail) {
		detailsRepository.save(timeDetail);
	}
	
	public void delete(int id) {
		detailsRepository.deleteById(id);
	}

}
